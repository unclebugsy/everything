
var e = {
    init: function() {
        $.getJSON('/ajax/master', function(nod) { $('#master').load('/ajax/node/get/' + nod.result )});
        console.log("init...");
    },
    insert: function(parentnode,parentelement) {
        console.log("Loading...");
        $.ajax({ type: "POST",
                 url: "/ajax/node/new",
                 data: { "parent": parentnode },
                 dataType: 'json',
                 failure: function(e) {
                     console.log(e);
                 },
                 success: function(dat) {
                     console.log("Yes.");
                     var nnid = dat.nuid;
                     console.log("nuid: " + nnid);
                     $(parentelement).append($('<div id=' + nnid + '>'));
                     console.log("Fetching.");
                     $('#' + nnid).load('/ajax/node/get/' + nnid);
                     console.log("Done?");
                 }
               });
                 
    }
};

$(document).ready(function() {
    e.init();
});


