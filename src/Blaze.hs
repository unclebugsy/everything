{-# LANGUAGE OverloadedStrings #-}

module Blaze where

import qualified Data.Text                   as T
import qualified Data.Text.Encoding          as T
import           Text.Blaze.Html5            (Attribute, AttributeValue, Html,
                                              (!))
import qualified Text.Blaze.Html5            as H
import qualified Text.Blaze.Html5.Attributes as A


irow :: Html -> Html
irow a = H.div ! A.class_ "row" $ a

idiv :: T.Text -> Html -> Html
idiv width cont =
  H.div ! A.class_ (H.toValue $ "span" `T.append` width) $
    cont

iriv :: T.Text -> Html -> Html
iriv wid cont =
  irow $ idiv wid cont

