{-# LANGUAGE OverloadedStrings #-}

module Site ( app ) where
import           Data.ByteString                             (ByteString)

import           Snap.Core
import           Snap.Snaplet
import           Snap.Snaplet.Session.Backends.CookieSession
import           Snap.Util.FileServe

import           Application
import           Pages

routes :: [(ByteString, Handler App App ())]
routes = [ ("", ifTop index)

         , ("/ajax/master", aj_get_master )
         , ("/ajax/node/get/:nodeid", aj_get_node)
         , ("/ajax/node/new", aj_new_node)
         , ("", serveDirectory "resources/static") ]

app :: SnapletInit App App
app = makeSnaplet "app" "An snaplet example application." Nothing $ do
  s <- nestSnaplet "sess" sess $
         initCookieSessionManager "site_key.txt" "sess" (Just 3600)
  addRoutes routes
  return $ App s
