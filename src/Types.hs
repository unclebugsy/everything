{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE TemplateHaskell    #-}

module Types where

import           Control.Applicative
import           Control.Monad       (liftM)
import           Data.Aeson          hiding (String, Value)
import           Data.Aeson.TH
import qualified Data.Text           as T
import           Data.Typeable
import           Data.UUID.V4        (nextRandom)
import           Database.MongoDB hiding (value)
import           GHC.Generics
import           Text.Blaze          (ToMarkup (..))

docstore :: Database
docstore = "documents"

uuid :: IO T.Text
uuid = do
  lx <- liftM (T.pack . show) nextRandom
  return $ T.concat $ T.splitOn "-" lx

class EMongo a where
  eput :: a -> Document
  eget :: Document -> Maybe a


data NType = NText
           | NMaster
           deriving (Eq,Typeable,Generic)

instance Show NType where
  show NText = "Text Node"
  show NMaster = "Master Node"

instance Val NType where
  val NText = String "NText"
  val NMaster = String "NMaster"
  cast' (String "NMaster") = Just NMaster
  cast' (String "NText") = Just NText
  cast' _ = Nothing

instance ToJSON NType
instance FromJSON NType

instance ToMarkup NType where
  toMarkup NMaster = "NMaster"
  toMarkup NText = "NText"

type Nodeid = T.Text

data Node = Node { nuid   :: Nodeid
                 , ntitle :: T.Text
                 , ndata  :: T.Text
                 , nnext  :: [Nodeid]
                 , ntype  :: NType
                 , nmongo :: Maybe ObjectId }
            deriving (Generic, Show)

$(deriveJSON id ''ObjectId)


instance ToJSON Node
instance FromJSON Node

instance EMongo Node where
  eput (Node u t d n ty mo) =
    case mo of
      Nothing ->
        [ ( "uid" =: u)
        , ( "title" =: t)
        , ( "data" =: d)
        , ( "next" =: n)
        , ( "type" =: ty) ]
      Just nnuid ->
        [ ( "uid" =: u)
        , ( "title" =: t)
        , ( "data" =: d)
        , ( "next" =: n)
        , ( "type" =: ty)
        , ( "_id" =: nnuid )]

  eget doc =
    case (,,,,,) <$> look "uid" doc
         <*> look "title" doc
         <*> look "data" doc
         <*> look "next" doc
         <*> look "type" doc
         <*> look "_id" doc of
      Nothing -> Nothing
      Just (u,t,d,n,ty,mo) ->
        Just $ Node (typed u) (typed t) (typed d) (typed n) (typed ty) (typed mo)

nadd_child :: Nodeid -> Nodeid -> IO (Either Failure ())
nadd_child parentid childid = do
  Just parent <- efetch parentid
  let children = nnext parent
  ewrite $ parent { nnext = children ++ [childid] }

new_node :: Nodeid -> IO Node
new_node parent = do
  nid <- uuid
  let nn = Node { nuid = nid
                , ntitle = "New Node"
                , ndata = T.empty
                , nnext = []
                , ntype = NText
                , nmongo = Nothing }
  _ <- nadd_child parent nid
  return nn

run :: Action IO a -> IO (Either Failure a)
run a = do
  p <- runIOE $ connect (host "127.0.0.1")
  res <- access p master "everything" a
  close p
  return res

ewrite :: EMongo a => a -> IO (Either Failure ())
ewrite thnod = run $ save docstore $ eput thnod

efetch :: T.Text -> IO (Maybe Node)
efetch nid = do
  the_node <- run $ find (select ["uid" =: nid] "documents") >>= rest
  case the_node of
    Right [lx] -> return $ eget lx
    _ -> return Nothing

find_master_or_new :: IO Node
find_master_or_new = do
  lx <- run $ find (select ["type" =: ("NMaster" :: String)] docstore) >>= rest
  case lx of
    Right [x] ->
      case eget x of
        Nothing -> error "THE IMPOSSIBLE HAPPENED."
        Just ff -> return ff
    _ -> do
      nid <- uuid
      let newnode = Node { nuid = nid
                         , ntitle = "New Node"
                         , ndata = T.empty
                         , nnext = []
                         , ntype = NMaster
                         , nmongo = Nothing }
      _ <- ewrite newnode
      return newnode
