{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Pages ( aj_get_master
             , aj_get_node
             , aj_new_node
             , Page(..)
             , index ) where

import           Control.Monad               (forM_)
import           Control.Monad.Trans         (liftIO)

-- Remember this?
--import           Data.Monoid                 (mconcat, mempty)

import           Data.Monoid                 (mempty)

import           Data.Aeson
import           Data.Default                (Default (..))
import qualified Data.Text                   as T
import qualified Data.Text.Encoding          as T

import           Snap.Core
import           Snap.Extras.JSON

import           Text.Blaze.Html5            (Attribute, AttributeValue, Html,
                                              (!))
import qualified Text.Blaze.Html5            as H
import qualified Text.Blaze.Html5.Attributes as A
import qualified Text.Blaze.Renderer.Pretty as BP
import qualified Text.Blaze.Renderer.Text as BT

import           Application
import           Blaze
import           Types



data Page = Page { page_header    :: Html
                 , page_toolbar   :: Html
                 , page_container :: (Html -> Html)
                 , page_content   :: Html }

instance Default Page where
  def = Page pi_header pi_toolbar pi_container mempty
    where
      font_string :: AttributeValue
      font_string = "http://fonts.googleapis.com/css?family=Ubuntu"

      pi_container :: Html -> Html
      pi_container cont =
        H.body $ do
          pi_toolbar
          H.div ! A.class_ "container" ! A.style "margin-top:10px;" $
            cont


      pi_toolbar :: Html
      pi_toolbar =
        H.div ! A.class_ "navbar navbar-static-top" $
          H.div ! A.class_ "navbar-inner" $ do
            H.a ! A.class_ "brand" ! A.href "#" $ "Everything"
            H.ul ! A.class_ "nav" $ do
              H.li ! A.class_ "active" $ H.a ! A.href "#" $ "Top"

      viewport_settings :: Attribute
      viewport_settings =
        A.content "width=device-width, initial-scale=1.0"

      pi_header :: Html
      pi_header =
        H.head $ do
          H.meta ! A.charset "utf-8"
          H.meta ! A.name "viewport" ! viewport_settings
          H.link ! A.type_ "text/css" ! A.rel "stylesheet" ! A.href "/bs/css/bootstrap.css"
          H.link ! A.type_ "text/css" ! A.rel "stylesheet" ! A.href "/bs/css/bootstrap-responsive.css"
          H.link ! A.type_ "text/css" ! A.rel "stylesheet" ! A.href font_string
          -- H.link ! A.href "/e.css" ! A.rel "stylesheet" ! A.type_ "text/css"
          H.script
            ! A.type_ "text/javascript"
            ! A.src "/bs/js/jquery.js" $ mempty
          H.script
            ! A.type_ "text/javascript"
            ! A.src "/underscore.js" $ mempty
          H.script
            ! A.type_ "text/javascript"
            ! A.src "/e.js" $ mempty
          H.script
            ! A.type_ "text/javascript"
            ! A.src "/bs/js/bootstrap.js" $ mempty
          H.script
            ! A.type_ "text/javascript"
            ! A.src "/bs/js/application.js" $ mempty
          H.script
            ! A.type_ "text/javascript"
            ! A.src "/bs/js/bootstrap-dropdown.js" $ mempty

blaze :: MonadSnap m => H.Markup -> m ()
blaze a = do
  modifyResponse $ addHeader "Content-Type" "text/html; charset=UTF-8"
  Snap.Core.writeText $ T.pack $ BP.renderHtml a

render :: Page -> AppHandler ()
render the_page = do
  blaze $
    H.html $ do
      (page_header the_page)
      (page_container the_page)
        (page_content the_page)

ajax :: Html -> AppHandler ()
ajax cont = blaze $ cont

index :: AppHandler ()
index = do
  render $ def { page_content = the_page }
 where
   the_page = do
      H.div ! A.class_ "row" ! A.style "clear:both;" $
        H.div ! A.id "master" $ mempty

render_node :: Node -> Html
render_node node =
  iriv "8" $
    H.table ! A.class_ "table-striped table-condensed table-bordered" $ do
      H.tr $ do
        H.td $ "Title"
        H.td $ H.toMarkup (ntitle node)
      H.tr $ do
        H.td $ "UID"
        H.td $ H.toMarkup $ (nuid node)
      H.tr $ do
        H.td $ "Type"
        H.td $ H.p $ H.toMarkup $ (ntype node)
      H.tr $ do
        H.td $ "Children"
        H.td $ do
          H.ul $ forM_ (nnext node) rline -- (H.li . H.toMarkup)
          H.p ! A.class_ "pull-right" $ do
            H.i
              ! A.class_ "icon-pencil"
              ! A.id "fart"
              ! A.onclick (H.toValue $ "e.insert('" ++> (nuid node) ++> "', $(this).parent().parent())") $ mempty
          H.ul $ do
            H.li ! A.id (H.toValue $ nuid node) $ mempty
 where
   rline a = do
     H.div ! A.id (H.toValue a) $ mempty
     H.script $ H.toValue "$('#" ++> a ++> "').load('/node/get/" ++> a ++> "');"
       
(++>) :: T.Text -> T.Text -> T.Text
(++>) a b = a `T.append` b

aj_get_master :: AppHandler ()
aj_get_master = do
  nod <- liftIO $ find_master_or_new
  writeJSON $ object ["result" .= nuid nod]

aj_get_node :: AppHandler ()
aj_get_node = do
  nid <- getParam "nodeid"
  case nid of
    Nothing -> n_not_found "No param!"
    Just nnid -> do
      the_node <- liftIO $ efetch $ T.decodeUtf8 nnid
      case the_node of
        Nothing -> n_not_found "Not in database."
        Just real_node -> ajax $ render_node real_node
 where
   n_not_found msg = ajax $ H.p $ msg

aj_new_node :: AppHandler ()
aj_new_node = do
  Just parent <- getParam "parent"
  nnode <- liftIO $ do
    nn <- new_node (T.decodeUtf8 parent)
    ewrite nn
    return nn
  writeJSON $ object [ "elementid" .= (nuid nnode)
                     , "nuid" .= (nuid nnode) ]
